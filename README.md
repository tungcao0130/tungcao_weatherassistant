node -v
v16.13.0

yarn -v
1.22.19

npm -v
9.5.0

"react-native": "0.71.3",

macOS Monterey v12.0.1
Xcode Version 13.3 (13E113)

Android Studio Electric Eel | 2022.1.1 Patch 1

![W1!](./Other/images/w1.png)
![W2!](./Other/images/w2.png)
![W3!](./Other/images/w3.png)
![W4!](./Other/images/w4.png)