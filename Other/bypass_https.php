<?php
// This zipcode from the question
$default_zipcode = 30076;
$query = isset($_GET['query']) ? $_GET['query'] : $default_zipcode;
$access_key = '610acf4c1d203448cd6f671955c5e8aa';

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://api.weatherstack.com/current?access_key=' . $access_key . '&query=' . $query,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
