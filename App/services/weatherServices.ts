import axiosClient from './apiClient';
import {API_GET_CURRENT_WEATHER} from './api';

export function getCurrentWeather(query: string) {
  return axiosClient.get(`${API_GET_CURRENT_WEATHER}?query=${query}`);
}
