import axios from 'axios';
import NetInfo from '@react-native-community/netinfo';
import {BASE_URL} from './api';
import {AXIOS_TIMEOUT} from '../utils/config';

NetInfo.fetch().then(state => {
  console.log('Connection type', state.type);
  console.log('Is connected?', state.isConnected);
});

const axiosClient = axios.create({
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

//All request will wait 2 seconds before timeout
axiosClient.defaults.timeout = AXIOS_TIMEOUT;

axiosClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    let res = error.response;
    if (res?.status === 401) {
      console.log('Axios request error 401: ', res);
    }
    console.log('Looks like there was a problem. Status Code: ' + res?.status);
    return Promise.reject(error);
  },
);

export default axiosClient;
