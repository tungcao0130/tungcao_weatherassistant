/*
 * Network Security
 * Your APIs should always use SSL encryption.
 * SSL encryption protects against the requested data being read in plain text between when it leaves the server and before it reaches the client.
 * You’ll know the endpoint is secure, because it starts with https:// instead of http://.
 *
 * Reference links: https://reactnative.dev/docs/security
 *
 * So I use my bypass with php code. /Other/bypass_https.php
 */

// export const BASE_URL = 'http://api.weatherstack.com';
// export const API_GET_CURRENT_WEATHER = '/current?access_key=610acf4c1d203448cd6f671955c5e8aa&query=30076';

// BASE_URL will be improve with Config and env file
export const BASE_URL = 'https://hat.cafe';
export const API_GET_CURRENT_WEATHER = '/tungcao/bypass_https.php';
