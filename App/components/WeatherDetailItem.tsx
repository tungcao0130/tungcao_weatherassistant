import {Box, Text} from 'native-base';
import React from 'react';
import {styles} from '../styles';
import {colors} from '../theme/colors';

const WeatherDetailItem = (props: {
  name: string;
  value: number;
  unit: string;
}) => {
  const {name, value, unit} = props;
  return (
    <Box
      flex={1}
      m={1}
      px={2.5}
      py={1}
      rounded={'lg'}
      bg={colors.transparentGray}>
      <Text style={styles.weather_text_secondary}>{name}</Text>
      <Text style={styles.weather_text} fontSize={'xl'}>
        {value} {unit}
      </Text>
    </Box>
  );
};

export default WeatherDetailItem;
