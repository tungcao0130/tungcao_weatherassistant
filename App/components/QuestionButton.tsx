import {Button} from 'native-base';
import React from 'react';
import {colors} from '../theme/colors';

const QuestionButton = (props: {title: string; onPress: any}) => {
  const {title, onPress} = props;
  return (
    <Button
      onPress={onPress}
      m={1}
      size="sm"
      variant="outline"
      _text={{
        color: colors.white,
      }}>
      {title}
    </Button>
  );
};

export default QuestionButton;
