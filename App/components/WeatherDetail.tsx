import {Box, Flex} from 'native-base';
import React from 'react';
import WeatherDetailItem from './WeatherDetailItem';
import {useTranslation} from 'react-i18next';

const WeatherDetail = (props: {
  weather: {
    wind_speed: number;
    visibility: number;
    humidity: number;
    uv_index: number;
    pressure: number;
  };
}) => {
  const {weather} = props;
  const {t} = useTranslation();
  return (
    <Box>
      <Flex flexDirection={'row'}>
        <WeatherDetailItem
          name={t('weather.wind')}
          unit={t('weather.wind_unit')}
          value={weather?.wind_speed}
        />
        <WeatherDetailItem
          name={t('weather.pressure')}
          unit={t('weather.pressure_unit')}
          value={weather?.pressure}
        />
      </Flex>
      <Flex flexDirection={'row'}>
        <WeatherDetailItem
          name={t('weather.humidity')}
          unit={t('weather.humidity_unit')}
          value={weather?.humidity}
        />
        <WeatherDetailItem
          name={t('weather.uv_index')}
          unit={t('weather.uv_index_unit')}
          value={weather?.uv_index}
        />
        <WeatherDetailItem
          name={t('weather.visibility')}
          unit={t('weather.visibility_unit')}
          value={weather?.visibility}
        />
      </Flex>
    </Box>
  );
};

export default WeatherDetail;
