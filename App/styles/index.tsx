import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  imageSpash: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
  },
  weather_text: {
    color: 'white',
  },
  weather_text_secondary: {
    color: 'darkgray',
  },
  weather_image_background: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    resizeMode: 'cover',
  },
});

export {styles};
