const colors = {
  white: 'white',
  transparent: 'rgba(52, 52, 52, 0.8)',
  transparentGray: 'rgba(192,192,192,0.2)',
};

export {colors};
