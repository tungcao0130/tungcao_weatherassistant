import React, {useState} from 'react';
import {
  SafeAreaView,
  ImageBackground,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  Box,
  Center,
  Input,
  NativeBaseProvider,
  ScrollView,
  SearchIcon,
  Text,
} from 'native-base';
import {useTranslation} from 'react-i18next';

// import common styles
import {styles} from '../../styles';

// import services
import {getCurrentWeather} from '../../services/weatherServices';

// import utils
import {
  WEATHER_API_RESPONSE_DEFAULT,
  WEATHER_CODE,
  QUESTIONS,
  QUESTION_CODE,
} from '../../utils/constant';

// import theme
import {colors} from '../../theme/colors';

// import component
import QuestionButton from '../../components/QuestionButton';
import WeatherDetail from '../../components/WeatherDetail';

// import images
import imageWeatherBackground from 'images/weather_background.jpg';

const HomeScreen = () => {
  const {t} = useTranslation();
  const [zipCode, setZipCode] = useState('ho chi minh');
  const [weather, setWeather] = useState(WEATHER_API_RESPONSE_DEFAULT);
  const [loading, setLoading] = useState(false);

  // #region call API
  const handleGetWeatherByZipCode = () => {
    if (zipCode.length > 2) {
      setLoading(true);
      getCurrentWeather(zipCode).then(response => {
        setLoading(false);
        if (response?.data?.success === false) {
          Alert.alert('', response?.data?.error?.info);
        } else {
          console.log(response?.data);
          setWeather(response?.data);
        }
      });
    }
  };
  // #endregion call API

  // #region function
  const userAskQuestion = (code: string) => {
    if (zipCode.length > 0 && weather?.current) {
      QUESTIONS.map(item => {
        if (item.code === code) {
          switch (item.code) {
            case QUESTION_CODE.go_out_side:
              Alert.alert(
                checkGoOut(weather?.current?.weather_code)
                  ? t('answers.go_out_side.yes')
                  : t('answers.go_out_side.no'),
              );
              break;
            case QUESTION_CODE.wear_sunscreen:
              Alert.alert(
                checkWearSunscreen(weather?.current?.uv_index)
                  ? t('answers.wear_sunscreen.yes')
                  : t('answers.wear_sunscreen.no'),
              );
              break;
            case QUESTION_CODE.fly_my_kite:
              Alert.alert(
                checkFlyMyKite(
                  weather?.current?.weather_code,
                  weather?.current?.wind_speed,
                )
                  ? t('answers.fly_my_kite.yes')
                  : t('answers.fly_my_kite.no'),
              );
              break;

            default:
              Alert.alert('NO CASE');
              break;
          }
        }
      });
    } else {
      Alert.alert(t('please_input_zipcode'));
    }
  };

  const checkIsRaining = (weatherCode: number) => {
    const isRaining = WEATHER_CODE.filter(w => w.code === weatherCode);
    return isRaining.length > 0;
  };

  const checkUvIndexAbove3 = (uvIndex: number) => {
    return uvIndex > 3;
  };

  const checkWindSpeedOver15 = (windSpeed: number) => {
    return windSpeed > 15;
  };

  const checkGoOut = (weatherCode: number) => {
    return !checkIsRaining(weatherCode);
  };

  const checkWearSunscreen = (uvIndex: number) => {
    return checkUvIndexAbove3(uvIndex);
  };

  const checkFlyMyKite = (weatherCode: number, windSpeed: number) => {
    return !checkIsRaining(weatherCode) && checkWindSpeedOver15(windSpeed);
  };
  // #endregion function

  return (
    <NativeBaseProvider>
      <ImageBackground
        style={styles.weather_image_background}
        source={imageWeatherBackground}>
        <SafeAreaView
          style={[styles.container, {backgroundColor: colors.transparent}]}>
          <ScrollView showsVerticalScrollIndicator={false} p={2}>
            <Box alignItems="center" mt="2.5">
              <Input
                color={'white'}
                width={'90%'}
                placeholder={t('input_zipcode').toString()}
                value={zipCode}
                keyboardType={'numbers-and-punctuation'}
                onChangeText={text => setZipCode(text)}
                onSubmitEditing={() => {
                  handleGetWeatherByZipCode();
                }}
                InputRightElement={
                  <TouchableOpacity
                    onPress={() => {
                      handleGetWeatherByZipCode();
                    }}>
                    <SearchIcon size="5" m="1" color="white" />
                  </TouchableOpacity>
                }
              />
            </Box>
            {!loading && (
              <Box>
                <Center my={2}>
                  <Text style={styles.weather_text} fontSize={'xl'}>
                    {weather?.location?.name}, {weather?.location?.country}
                  </Text>
                  <Text style={styles.weather_text} fontSize={'5xl'}>
                    {weather?.current?.temperature} {t('weather.degrees_unit')}
                  </Text>
                  <Text style={styles.weather_text} fontSize={'lg'}>
                    {weather?.current?.weather_descriptions.map(
                      element => `${element} `,
                    )}
                  </Text>
                </Center>

                <Box my={2}>
                  <WeatherDetail weather={weather?.current} />
                </Box>
                <Box my={2}>
                  <Box bg={colors.transparentGray} m={1} p={2} rounded={'lg'}>
                    {QUESTIONS.map(item => (
                      <QuestionButton
                        key={item.code}
                        title={item.title}
                        onPress={() => {
                          userAskQuestion(item.code);
                        }}
                      />
                    ))}
                  </Box>
                </Box>
              </Box>
            )}
          </ScrollView>
        </SafeAreaView>
      </ImageBackground>
    </NativeBaseProvider>
  );
};

export default HomeScreen;
