import React, {useEffect} from 'react';
import {Image, SafeAreaView} from 'react-native';
import imageSplash from 'images/splash.png';
import {styles} from '../../styles';

const SplashScreen = ({navigation}: {navigation: any}) => {
  useEffect(() => {
    // Will check authen here
    // I add the setTimeout to show the splash screen
    setTimeout(() => {
      navigation.replace('Home');
    }, 1000);
  }, [navigation]);

  return (
    <SafeAreaView>
      <Image source={imageSplash} style={styles.imageSpash} />
    </SafeAreaView>
  );
};

export default SplashScreen;
