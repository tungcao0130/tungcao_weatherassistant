import i18next from '../i18n/i18n';

// Weather code from weatherstack.com
// https://weatherstack.com/site_resources/weatherstack-weather-condition-codes.zip
// I just collect only raining
const WEATHER_CODE = [
  {code: 176, description: 'Patchy rain possible'},
  {code: 185, description: 'Patchy freezing drizzle possible'},
  {code: 263, description: 'Patchy light drizzle'},
  {code: 266, description: 'Light drizzle'},
  {code: 281, description: 'Freezing drizzle'},
  {code: 284, description: 'Heavy freezing drizzle'},
  {code: 293, description: 'Patchy light rain'},
  {code: 296, description: 'Light rain'},
  {code: 299, description: 'Moderate rain at times'},
  {code: 302, description: 'Moderate rain'},
  {code: 305, description: 'Heavy rain at times'},
  {code: 308, description: 'Heavy rain'},
  {code: 311, description: 'Light freezing rain'},
];

// copy from the response of the API http://api.weatherstack.com/current?access_key=610acf4c1d203448cd6f671955c5e8aa&query=30076
// because has limited :(
// Your monthly usage limit has been reached. Please upgrade your Subscription Plan
const WEATHER_API_RESPONSE_DEFAULT = {
  current: {
    cloudcover: 50,
    feelslike: 27,
    humidity: 74,
    is_day: 'no',
    observation_time: '03:12 PM',
    precip: 0,
    pressure: 1012,
    temperature: 26,
    uv_index: 1,
    visibility: 10,
    weather_code: 116,
    weather_descriptions: ['Partly cloudy'],
    weather_icons: [
      'https://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png',
    ],
    wind_degree: 110,
    wind_dir: 'ESE',
    wind_speed: 7,
  },
  location: {
    country: 'Vietnam',
    lat: '10.750',
    localtime: '2023-02-24 22:12',
    localtime_epoch: 1677276720,
    lon: '106.667',
    name: 'Ho Chi Minh City',
    region: '',
    timezone_id: 'Asia/Ho_Chi_Minh',
    utc_offset: '7.0',
  },
  request: {
    language: 'en',
    query: 'Ho Chi Minh City, Vietnam',
    type: 'City',
    unit: 'm',
  },
};

const QUESTION_CODE = {
  go_out_side: 'go_out_side',
  wear_sunscreen: 'wear_sunscreen',
  fly_my_kite: 'fly_my_kite',
};

const QUESTIONS = [
  {
    title: i18next.t('questions.go_out_side'),
    code: QUESTION_CODE.go_out_side,
  },
  {
    title: i18next.t('questions.wear_sunscreen'),
    code: QUESTION_CODE.wear_sunscreen,
  },
  {
    title: i18next.t('questions.fly_my_kite'),
    code: QUESTION_CODE.fly_my_kite,
  },
];

export {WEATHER_CODE, WEATHER_API_RESPONSE_DEFAULT, QUESTIONS, QUESTION_CODE};
