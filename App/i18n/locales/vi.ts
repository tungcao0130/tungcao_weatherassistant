export default {
  greeting: 'Xin chào!',
  input_zipcode: 'Nhập zipcode: 30076',
  please_input_zipcode: 'Vui lòng nhập zipcode và tìm kiếm',
  view: 'Xem',
  weather: {
    wind: 'Gió',
    pressure: 'Áp suất',
    humidity: 'Độ ẩm',
    uv_index: 'Chỉ số UV',
    visibility: 'Tầm nhìn',

    wind_unit: 'km/h',
    pressure_unit: 'mb',
    humidity_unit: '%',
    uv_index_unit: '/ 10',
    visibility_unit: 'km',

    degrees_unit: '°',
  },
  http: {
    network_timeout: 'Quá thời gian kết nối',
    network_error: 'Lỗi mạng',
  },
  questions: {
    go_out_side: 'Tôi có nên đi ra ngoài không?',
    wear_sunscreen: 'Tôi có nên bôi kem chống nắng không??',
    fly_my_kite: 'Tôi có thể thả diều không?',
  },
  answers: {
    go_out_side: {
      yes: 'Bạn có thể ra ngoài vì trời không mưa',
      no: 'Bạn không thể ra ngoài vì trời đang mưa',
    },
    wear_sunscreen: {
      yes: 'Bạn nên bôi kem chống nắng',
      no: 'Bạn không cần bôi kem chống nắng',
    },
    fly_my_kite: {
      yes: 'Bạn có thể thả diều',
      no: 'Bạn không thể thả diều',
    },
  },
};
