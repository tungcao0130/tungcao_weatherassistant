export default {
  greeting: 'Hi!',
  input_zipcode: 'Input your zipcode: 30076',
  please_input_zipcode: 'Please input zipcode and search',
  view: 'View',
  weather: {
    wind: 'Wind',
    pressure: 'Pressure',
    humidity: 'Humidity',
    uv_index: 'UV Index',
    visibility: 'Visibility',

    wind_unit: 'km/h',
    pressure_unit: 'mb',
    humidity_unit: '%',
    uv_index_unit: 'of 10',
    visibility_unit: 'km',

    degrees_unit: '°',
  },
  http: {
    network_timeout: 'Network timeout',
    network_error: 'Network error',
  },
  questions: {
    go_out_side: 'Should I go outside?',
    wear_sunscreen: 'Should I wear sunscreen?',
    fly_my_kite: 'Can I fly my kite?',
  },
  answers: {
    go_out_side: {
      yes: "YES. You can go out because it's not raining",
      no: "NO. You can't go out because it's raining",
    },
    wear_sunscreen: {
      yes: 'YES. You should wear sunscreen',
      no: "NO. You don't need to wear sunscreen",
    },
    fly_my_kite: {
      yes: 'YES. You can fly a kite',
      no: "NO. You can't fly a kite",
    },
  },
};
